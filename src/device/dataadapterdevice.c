/*
 * This file is part of libphidget22
 *
 * Copyright 2015 Phidgets Inc <patrick@phidgets.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see
 * <http://www.gnu.org/licenses/>
 */

#include "phidgetbase.h"
#include "gpp.h"
#include "device/dataadapterdevice.h"
#include "class/dataadapter.gen.h"

// === Internal Functions === //

//initAfterOpen - sets up the initial state of an object, reading in packets from the device if needed
//				  used during attach initialization - on every attach
static PhidgetReturnCode CCONV
PhidgetDataAdapterDevice_initAfterOpen(PhidgetDeviceHandle device) {

	PhidgetDataAdapterDeviceHandle phid = (PhidgetDataAdapterDeviceHandle)device;
	//PhidgetReturnCode ret;
	assert(phid);

	switch (device->deviceInfo.UDD->uid) {
	case PHIDUID_ADP_RS232:
		phid->packetCount = 0;
		for (int i = 0; i < DATAADAPTER_MAXINPUTS; i++)
			phid->inputState[i] = PUNK_BOOL;
		break;
	case PHIDUID_ADP_RS485_422:
		phid->packetCount = 0;
		phid->storedPacketLength = 0;
		break;
	case PHIDUID_ADP_SERIAL:
		phid->packetCount = 0;
		phid->storedPacketLength = 0;
		phid->protocol = PROTOCOL_SPI;
		for (int i = 0; i < DATAADAPTER_MAXINPUTS; i++)
			phid->inputState[i] = PUNK_BOOL;
		break;
	default:
		MOS_PANIC("Unexpected device");
	}

	return (EPHIDGET_OK);
}

//dataInput - parses device packets
static PhidgetReturnCode CCONV
PhidgetDataAdapterDevice_dataInput(PhidgetDeviceHandle device, uint8_t *buffer, size_t length) {
	PhidgetDataAdapterDeviceHandle phid = (PhidgetDataAdapterDeviceHandle)device;
	PhidgetChannelHandle channel;
	uint8_t lastInputState[DATAADAPTER_MAXINPUTS];
	uint16_t receivedPacketCount;
	PhidgetReturnCode ret;
	int endPacket = 0;
	int packetOverrun = 0;
	int error = 0;
	int i, j;

	assert(phid);
	assert(buffer);

	//Parse device packets - store data locally
	switch (device->deviceInfo.UDD->uid) {
	case PHIDUID_ADP_RS232:
		if (length > 0) {
			switch (buffer[0]) {
			case DATAADAPTER_PACKET_DATA_ERROR:
				error = 1;  //we intend to fall through to the next here
			case DATAADAPTER_PACKET_DATA_END:
				endPacket = 1;
			case DATAADAPTER_PACKET_DATA:
				receivedPacketCount = unpack16(&buffer[1]);
				if (phid->packetCount != receivedPacketCount) {
					error = 1;
				}
				phid->packetCount = receivedPacketCount + 1;
				memcpy(phid->lastData, buffer + 3, length - 3);

				phid->lastDataLength = phid->storedPacketLength;
				if (phid->storedPacketLength + length - 3 <= 8192) {
					memcpy(&phid->storedPacket[phid->storedPacketLength], phid->lastData, length - 3);
					phid->storedPacketLength += (uint16_t)length - 3;
				//	if (endPacket == 0)
				//		return EPHIDGET_OK;
				} else
					packetOverrun = 1;

				if ((channel = getChannel(phid, 0)) != NULL) {
					ret = bridgeSendToChannel(channel, BP_DATAIN, "%*R%u", phid->storedPacketLength, phid->storedPacket, error);

					if (ret != EPHIDGET_NOSPC)
						phid->storedPacketLength = 0;

					PhidgetRelease(&channel);
				}

				if (packetOverrun) {
					memcpy(&phid->storedPacket[phid->storedPacketLength], phid->lastData, length - 3);
					phid->storedPacketLength += (uint16_t)length - 3;
				}

				return (EPHIDGET_OK);
			case DATAADAPTER_PACKET_DROPPED:
				if ((channel = getChannel(phid, 0)) != NULL) {
					SEND_ERROR_EVENT(channel, EEPHIDGET_PACKETLOST, "One or more of the transmitted packets were lost.");
					PhidgetRelease(&channel);
				}
				return (EPHIDGET_OK);
			case STATE_CHANGE:
				for (j = 0; j < phid->devChannelCnts.numInputs; j++) {
	//				inputState[j] = PUNK_BOOL;
					lastInputState[j] = phid->inputState[j];
					phid->inputState[j] = ((buffer[1] & (1 << j)) != 0);
				}

				for (i = 0; i < phid->devChannelCnts.numInputs; i++) {
					int chIndex = i + phid->devChannelCnts.numDataAdapters;
					if ((channel = getChannel(phid, chIndex)) != NULL) {
						if (phid->inputState[i] != PUNK_BOOL && phid->inputState[i] != lastInputState[i]) {
							bridgeSendToChannel(channel, BP_STATECHANGE, "%d", (int)(phid->inputState[i]));
						}
						PhidgetRelease(&channel);
					}
				}

				return (EPHIDGET_OK);
			default:
				MOS_PANIC("Unexpected packet type");
			}
		}
		MOS_PANIC("Unexpected packet type");
	case PHIDUID_ADP_RS485_422:
		if (length > 0) {
			switch (buffer[0]) {
			case DATAADAPTER_PACKET_DATA_ERROR:
				error = 1;  //we intend to fall through to the next here
			case DATAADAPTER_PACKET_DATA_END:
				endPacket = 1;
			case DATAADAPTER_PACKET_DATA:
				receivedPacketCount = unpack16(&buffer[1]);
				if (phid->packetCount != receivedPacketCount) {
					error = 1;
				}
				phid->packetCount = receivedPacketCount + 1;
				memcpy(phid->lastData, buffer + 3, length - 3);

				phid->lastDataLength = phid->storedPacketLength;
				if (phid->storedPacketLength + length - 3 <= 8192) {
					memcpy(&phid->storedPacket[phid->storedPacketLength], phid->lastData, length - 3);
					phid->storedPacketLength += (uint16_t)length - 3;
					if ((endPacket == 0) && (phid->protocol != PROTOCOL_RS422 && phid->protocol != PROTOCOL_RS485))
						return EPHIDGET_OK;
				} else
					packetOverrun = 1;

				if ((channel = getChannel(phid, 0)) != NULL) {
					ret = bridgeSendToChannel(channel, BP_DATAIN, "%*R%u", phid->storedPacketLength, phid->storedPacket, error);

					if (ret != EPHIDGET_NOSPC)
						phid->storedPacketLength = 0;

					PhidgetRelease(&channel);
				}

				if (packetOverrun) {
					memcpy(&phid->storedPacket[phid->storedPacketLength], phid->lastData, length - 3);
					phid->storedPacketLength += (uint16_t)length - 3;
				}

				return (EPHIDGET_OK);
			case DATAADAPTER_PACKET_DROPPED:
				if ((channel = getChannel(phid, 0)) != NULL) {
					SEND_ERROR_EVENT(channel, EEPHIDGET_PACKETLOST, "One or more of the transmitted packets were lost.");
					PhidgetRelease(&channel);
				}
				return (EPHIDGET_OK);
			default:
				MOS_PANIC("Unexpected packet type");
			}
		}
		MOS_PANIC("Unexpected packet type");
	case PHIDUID_ADP_SERIAL:
		if (length > 0) {
			switch (buffer[0]) {
			case DATAADAPTER_PACKET_DATA_ERROR:
				error = 1;  //we intend to fall through to the next here
			case DATAADAPTER_PACKET_DATA_END:
				endPacket = 1;
			case DATAADAPTER_PACKET_DATA:
				receivedPacketCount = unpack16(&buffer[1]);
				if (phid->packetCount != receivedPacketCount) {
					error = 1;
				}
				phid->packetCount = receivedPacketCount + 1;
				memcpy(phid->lastData, buffer + 3, length - 3);

				phid->lastDataLength = phid->storedPacketLength;
				if (phid->storedPacketLength + length - 3 <= 8192) {
					memcpy(&phid->storedPacket[phid->storedPacketLength], phid->lastData, length - 3);
					phid->storedPacketLength += (uint16_t)length - 3;
					if ((endPacket == 0) && (phid->protocol != PROTOCOL_UART))
						return EPHIDGET_OK;
				} else
					packetOverrun = 1;

				if ((channel = getChannel(phid, 0)) != NULL) {
					ret = bridgeSendToChannel(channel, BP_DATAIN, "%*R%u", (int)phid->storedPacketLength, phid->storedPacket, error);
					if (ret != EPHIDGET_NOSPC)
						phid->storedPacketLength = 0;
					//else
					//	MOS_PANIC("TODO: Appending data won't work in this context");

					PhidgetRelease(&channel);
				}

				if (packetOverrun) {
					memcpy(&phid->storedPacket[phid->storedPacketLength], phid->lastData, length - 3);
					phid->storedPacketLength += (uint16_t)length - 3;
				}

				return (EPHIDGET_OK);
			case DATAADAPTER_PACKET_DROPPED:
				if ((channel = getChannel(phid, 0)) != NULL) {
					SEND_ERROR_EVENT(channel, EEPHIDGET_PACKETLOST, "One or more of the transmitted packets were lost.");
					PhidgetRelease(&channel);
				}
				return (EPHIDGET_OK);
			case DATAADAPTER_VOLTAGE_ERROR:
				if ((channel = getChannel(phid, 0)) != NULL) {
					SEND_ERROR_EVENT(channel, EEPHIDGET_VOLTAGEERROR, "Voltage Error Detected");
					PhidgetRelease(&channel);
				}
				return (EPHIDGET_OK);
			case STATE_CHANGE:
				for (j = 0; j < phid->devChannelCnts.numInputs; j++) {
					//				inputState[j] = PUNK_BOOL;
					lastInputState[j] = phid->inputState[j];
					phid->inputState[j] = ((buffer[1] & (1 << j)) != 0);
				}

				for (i = 0; i < phid->devChannelCnts.numInputs; i++) {
					int chIndex = i + phid->devChannelCnts.numDataAdapters;
					if ((channel = getChannel(phid, chIndex)) != NULL) {
						if (phid->inputState[i] != PUNK_BOOL && phid->inputState[i] != lastInputState[i]) {
							bridgeSendToChannel(channel, BP_STATECHANGE, "%d", (int)(phid->inputState[i]));
						}
						PhidgetRelease(&channel);
					}
				}
				return (EPHIDGET_OK);
			default:
				MOS_PANIC("Unexpected packet type");
			}
		}
		MOS_PANIC("Unexpected packet type");
	default:
		MOS_PANIC("Unexpected device");
	}

}

static PhidgetReturnCode CCONV
PhidgetDataAdapterDevice_bridgeInput(PhidgetChannelHandle ch, BridgePacket *bp) {
	PhidgetDataAdapterDeviceHandle phid = (PhidgetDataAdapterDeviceHandle)ch->parent;
	unsigned char buffer[MAX_OUT_PACKET_SIZE] = { 0 };
	PhidgetReturnCode ret;
	double dutyCycle;
	int32_t state;
	size_t len;

	assert(phid->phid.deviceInfo.class == PHIDCLASS_DATAADAPTER);


	switch (((PhidgetDeviceHandle)phid)->deviceInfo.UDD->uid) {
	case PHIDUID_ADP_RS232:
		switch (ch->class) {
		case PHIDCHCLASS_DATAADAPTER:
			switch (bp->vpkt) {
			case BP_DATAOUT:
				ret = sendData(phid, bp);
				return ret;
			case BP_SETBAUDRATE:
				len = 4;
				pack32(buffer, getBridgePacketUInt32(bp, 0));
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_BAUD_RATE, 0, buffer, &len, 100);
			case BP_SETPARITY:
				len = 1;
				buffer[0] = (uint8_t)getBridgePacketInt32(bp, 0);
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_PARITY_MODE, 0, buffer, &len, 100);
			case BP_SETSTOPBITS:
				len = 1;
				buffer[0] = (uint8_t)getBridgePacketInt32(bp, 0);
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_STOP_BITS, 0, buffer, &len, 100);
			case BP_SETDATABITS:
				len = 1;
				buffer[0] = (uint8_t)getBridgePacketUInt32(bp, 0);
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_DATA_BITS, 0, buffer, &len, 100);
			case BP_SETHANDSHAKEMODE:
				len = 1;
				buffer[0] = (uint8_t)getBridgePacketInt32(bp, 0);
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_RTS_CTS_MODE, 0, buffer, &len, 100);
			case BP_OPENRESET:
			case BP_CLOSERESET:
				len = 0;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_RESET, 0, buffer, &len, 100);
			case BP_ENABLE:
				len = 0;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_ENABLE, 0, buffer, &len, 100);
			default:
				MOS_PANIC("Unexpected packet type");
			}
		case PHIDCHCLASS_DIGITALINPUT:
			switch (bp->vpkt) {
			case BP_OPENRESET:
			case BP_CLOSERESET:
				len = 0;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_CHANNEL_WRITE, VINT_PACKET_TYPE_PHIDGET_RESET, ch->uniqueIndex, buffer, &len, 100);
			case BP_ENABLE:
				len = 0;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_CHANNEL_WRITE, VINT_PACKET_TYPE_PHIDGET_ENABLE, ch->uniqueIndex, buffer, &len, 100);
			default:
				MOS_PANIC("Unexpected packet type");
			}
		case PHIDCHCLASS_DIGITALOUTPUT:
			switch (bp->vpkt) {
			case BP_SETDUTYCYCLE:
				dutyCycle = getBridgePacketDouble(bp, 0);
				if (dutyCycle != 0 && dutyCycle != 1)
					return EPHIDGET_INVALIDARG;
				buffer[0] = (uint8_t)dutyCycle;
				len = 1;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_CHANNEL_WRITE, VINT_PACKET_TYPE_DIGITALOUTPUT_SETDUTYCYCLE, ch->uniqueIndex, buffer, &len, 100);
			case BP_SETSTATE:
				state = getBridgePacketInt32(bp, 0);
				if (state != 0 && state != 1)
					return EPHIDGET_INVALIDARG;
				buffer[0] = (uint8_t)state;
				len = 1;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_CHANNEL_WRITE, VINT_PACKET_TYPE_DIGITALOUTPUT_SETDUTYCYCLE, ch->uniqueIndex, buffer, &len, 100);
			case BP_OPENRESET:
			case BP_CLOSERESET:
				len = 0;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_CHANNEL_WRITE, VINT_PACKET_TYPE_PHIDGET_RESET, ch->uniqueIndex, buffer, &len, 100);
			case BP_ENABLE:
				len = 0;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_CHANNEL_WRITE, VINT_PACKET_TYPE_PHIDGET_ENABLE, ch->uniqueIndex, buffer, &len, 100);
			default:
				MOS_PANIC("Unexpected packet type");
			}
		default:
			MOS_PANIC("Unexpected Channel Class");
		}
	case PHIDUID_ADP_RS485_422:
		switch (bp->vpkt) {
		case BP_DATAOUT:
			ret = sendData(phid, bp);

			return ret;
		case BP_SETBAUDRATE:
			len = 4;
			pack32(buffer, getBridgePacketUInt32(bp, 0));
			return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_BAUD_RATE, 0, buffer, &len, 100);
		case BP_SETPARITY:
			len = 1;
			buffer[0] = (uint8_t)getBridgePacketInt32(bp, 0);
			return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_PARITY_MODE, 0, buffer, &len, 100);
		case BP_SETSTOPBITS:
			len = 1;
			buffer[0] = (uint8_t)getBridgePacketInt32(bp, 0);
			return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_STOP_BITS, 0, buffer, &len, 100);
		case BP_SETDATABITS:
			len = 1;
			buffer[0] = (uint8_t)getBridgePacketUInt32(bp, 0);
			return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_DATA_BITS, 0, buffer, &len, 100);
		case BP_OPENRESET:
		case BP_CLOSERESET:
			len = 0;
			return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_RESET, 0, buffer, &len, 100);
		case BP_ENABLE:
			len = 0;
			return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_ENABLE, 0, buffer, &len, 100);

		case BP_SETPROTOCOL:
			phid->protocol = (uint8_t)getBridgePacketInt32(bp, 0);
			len = 1;
			buffer[0] = (uint8_t)getBridgePacketInt32(bp, 0);
			return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_PROTOCOL, 0, buffer, &len, 100);
		case BP_SETTIMEOUT:
			len = 2;
			pack16(&buffer[0], (uint16_t)getBridgePacketUInt32(bp, 0));
			return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_TIMEOUT, 0, buffer, &len, 100);
		default:
			MOS_PANIC("Unexpected packet type");
		}
	case PHIDUID_ADP_SERIAL:
		switch (ch->class) {
		case PHIDCHCLASS_DATAADAPTER:
			switch (bp->vpkt) {
			case BP_DATAOUT:
				if(phid->protocol != PROTOCOL_I2C)
					ret = sendData(phid, bp);
				else
					ret = sendI2CData(phid, bp);

				return ret;
			case BP_SETBAUDRATE:
				len = 4;
				pack32(buffer, getBridgePacketUInt32(bp, 0));
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_BAUD_RATE, 0, buffer, &len, 100);
			case BP_OPENRESET:
			case BP_CLOSERESET:
				len = 0;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_RESET, 0, buffer, &len, 100);
			case BP_ENABLE:
				len = 0;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_ENABLE, 0, buffer, &len, 100);

			case BP_SETDATABITS:
				len = 1;
				buffer[0] = (uint8_t)getBridgePacketUInt32(bp, 0);
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_DATA_BITS, 0, buffer, &len, 100);
			case BP_SETPROTOCOL:
				phid->protocol = (uint8_t)getBridgePacketInt32(bp, 0);
				len = 1;
				buffer[0] = (uint8_t)getBridgePacketInt32(bp, 0);
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_PROTOCOL, 0, buffer, &len, 100);
			case BP_SETHANDSHAKEMODE:
				len = 1;
				buffer[0] = (uint8_t)getBridgePacketInt32(bp, 0);
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_RTS_CTS_MODE, 0, buffer, &len, 100);
			case BP_SETSPIMODE:
				len = 1;
				buffer[0] = (uint8_t)getBridgePacketInt32(bp, 0);
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_SPIMODE, 0, buffer, &len, 100);
			case BP_SETADDRESS:
				if (phid->protocol != PROTOCOL_I2C) {
					len = 1;
					buffer[0] = (uint8_t)getBridgePacketUInt32(bp, 0);
					return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_ADDRESS, 0, buffer, &len, 100);
				}
				else {
					phid->address = getBridgePacketUInt32(bp, 0);
					return EPHIDGET_OK;
				}
			case BP_SETENDIANNESS:
				len = 1;
				buffer[0] = (uint8_t)getBridgePacketInt32(bp, 0);
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_ENDIANNESS, 0, buffer, &len, 100);
			case BP_SETIOVOLTAGE:
				len = 1;
				buffer[0] = (uint8_t)getBridgePacketInt32(bp, 0);
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_IOVOLTAGE, 0, buffer, &len, 100);
			case BP_SETTIMEOUT:
				len = 2;
				pack16(&buffer[0], (uint16_t)getBridgePacketUInt32(bp, 0));
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_DEVICE_WRITE, DATAADAPTER_TIMEOUT, 0, buffer, &len, 100);
			case BP_SETI2CFORMAT:
				//memcpy(phid->i2cFormatString, getBridgePacketString(bp, 0), mos_strlen(getBridgePacketString(bp, 0)));
				ret = parseI2CFormat(phid, getBridgePacketString(bp, 0));
				return ret;
			default:
				MOS_PANIC("Unexpected packet type");
			}
		case PHIDCHCLASS_DIGITALINPUT:
			switch (bp->vpkt) {
			case BP_OPENRESET:
			case BP_CLOSERESET:
				len = 0;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_CHANNEL_WRITE, VINT_PACKET_TYPE_PHIDGET_RESET, ch->uniqueIndex, buffer, &len, 100);
			case BP_ENABLE:
				len = 0;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_CHANNEL_WRITE, VINT_PACKET_TYPE_PHIDGET_ENABLE, ch->uniqueIndex, buffer, &len, 100);
			default:
				MOS_PANIC("Unexpected packet type");
			}
		case PHIDCHCLASS_DIGITALOUTPUT:
			switch (bp->vpkt) {
			case BP_SETDUTYCYCLE:
				dutyCycle = getBridgePacketDouble(bp, 0);
				if (dutyCycle != 0 && dutyCycle != 1)
					return EPHIDGET_INVALIDARG;
				buffer[0] = (uint8_t)dutyCycle;
				len = 1;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_CHANNEL_WRITE, VINT_PACKET_TYPE_DIGITALOUTPUT_SETDUTYCYCLE, ch->uniqueIndex, buffer, &len, 100);
			case BP_SETSTATE:
				state = getBridgePacketInt32(bp, 0);
				if (state != 0 && state != 1)
					return EPHIDGET_INVALIDARG;
				buffer[0] = (uint8_t)state;
				len = 1;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_CHANNEL_WRITE, VINT_PACKET_TYPE_DIGITALOUTPUT_SETDUTYCYCLE, ch->uniqueIndex, buffer, &len, 100);
			case BP_OPENRESET:
			case BP_CLOSERESET:
				len = 0;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_CHANNEL_WRITE, VINT_PACKET_TYPE_PHIDGET_RESET, ch->uniqueIndex, buffer, &len, 100);
			case BP_ENABLE:
				len = 0;
				return PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_CHANNEL_WRITE, VINT_PACKET_TYPE_PHIDGET_ENABLE, ch->uniqueIndex, buffer, &len, 100);
			default:
				MOS_PANIC("Unexpected packet type");
			}
		default:
			MOS_PANIC("Unexpected Channel Class");
		}
	default:
		MOS_PANIC("Unexpected device");
	}
}

static void CCONV
PhidgetDataAdapterDevice_free(PhidgetDeviceHandle *phid) {

	mos_free(*phid, sizeof(struct _PhidgetDataAdapterDevice));
	*phid = NULL;
}

PhidgetReturnCode
PhidgetDataAdapterDevice_create(PhidgetDataAdapterDeviceHandle *phidp) {
	DEVICECREATE_BODY(DataAdapterDevice, PHIDCLASS_DATAADAPTER);
	return (EPHIDGET_OK);
}


PhidgetReturnCode sendData( PhidgetDataAdapterDeviceHandle phid, BridgePacket* bp) {
	return sendDataBuffer(phid, bp->entry[0].len, (const uint8_t *)getBridgePacketUInt8Array(bp, 0) , bp);
}

PhidgetReturnCode parseI2CFormat(PhidgetDataAdapterDeviceHandle phid, const char *string) {
	int index = 0;
	int count = 0;
	int stopped = 0;
	int mode = 0; //0 transmit, 1 receive
	int totalFormatCount = 0;

	uint8_t tmpFormatList[128];

	for (size_t i = 0; i < mos_strlen(string); i++) {
		if (stopped)
			return EPHIDGET_INVALIDARG;
		switch (string[i]) {
		case 's':
			if (i == 0)
				continue;
			if (count != 0) {
				tmpFormatList[index] = count;
				if (mode)
					tmpFormatList[index] |= 0x80;

				index++;
				count = 0;
			} else
				return EPHIDGET_INVALIDARG;
			break;
		case 'T':
			if (i == 0)
				return EPHIDGET_INVALIDARG;
			if (count == 0) {
				mode = 0;
			}
			if (mode != 0) {
				return EPHIDGET_INVALIDARG;
			}
			count++;
			totalFormatCount++;
			if (count > 127 || totalFormatCount > 512)
				return EPHIDGET_INVALIDARG;
			break;
		case 'R':
			if (i == 0)
				return EPHIDGET_INVALIDARG;
			if (count == 0) {
				mode = 1;
			}
			if (mode != 1) {
				return EPHIDGET_INVALIDARG;
			}
			count++;
			totalFormatCount++;
			if (count > 127 || totalFormatCount > 512)
				return EPHIDGET_INVALIDARG;
			break;
		case 'p':
			if (i == 0)
				return EPHIDGET_INVALIDARG;
			if (count != 0) {
				tmpFormatList[index] = count;
				if (mode)
					tmpFormatList[index] |= 0x80;

				

			} else
				return EPHIDGET_INVALIDARG;
			stopped = 1;
			break;
		default:
			return EPHIDGET_INVALIDARG;
		}
	}
	if (!stopped)
		return EPHIDGET_INVALIDARG;

	phid->i2cFormatCount = index + 1;
	memcpy(phid->i2cFormatList, tmpFormatList, phid->i2cFormatCount);

	return EPHIDGET_OK;
}

PhidgetReturnCode sendI2CData(PhidgetDataAdapterDeviceHandle phid, BridgePacket* bp) {
	uint8_t buffer[1024];
	int transmitCount = 0;
	uint8_t dataSize = bp->entry[0].len;

	uint8_t totalCount = dataSize + phid->i2cFormatCount + 2;

	if (totalCount > 1024)
		return EPHIDGET_INVALIDARG;

	for (int i = 0; i < phid->i2cFormatCount; i++) {
		if (!(phid->i2cFormatList[i] & 0x80)) //if transmit segment
			transmitCount += phid->i2cFormatList[i] & 0x7F;
	}

	if (transmitCount != dataSize)
		return EPHIDGET_INVALIDARG;

	buffer[0] = phid->address;
	buffer[1] = phid->i2cFormatCount;
	memcpy(&buffer[2], phid->i2cFormatList, phid->i2cFormatCount);
	memcpy(&buffer[phid->i2cFormatCount+2], (const uint8_t *)getBridgePacketUInt8Array(bp, 0), dataSize);

	return sendDataBuffer(phid, totalCount, buffer, bp);
}

PhidgetReturnCode sendDataBuffer(PhidgetDataAdapterDeviceHandle phid, size_t len, const uint8_t *buffer, BridgePacket* bp) {
	PhidgetReturnCode ret;
	size_t packetLen;
	uint32_t packetCount = 0;
	size_t i = 0;

	uint8_t buf[64];

	ret = EPHIDGET_OK;

	if (len > 0) {
		if (len <= 59) {
			packetLen = len + 5;
			buf[0] = 1;
			pack32(&buf[1], (uint32_t)len);
			memcpy(&buf[5], buffer, len);
			ret = PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_BULK_WRITE, 0, 0, buf, &packetLen, 5000);
			if (ret != EPHIDGET_OK)
				return ret;

		} else {
			for (i = 0; i + 59 < len; i += 59) {
				packetLen = 64;
				if (i == 0) {
					buf[0] = 1;
					pack32(&buf[1], (uint32_t)len);
				} else {
					buf[0] = 0;
					pack32(&buf[1], (uint32_t)packetCount);
				}

				memcpy(&buf[5], buffer + i, packetLen - 5);
				ret = PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_BULK_WRITE, 0, 0, buf, &packetLen, 5000);
				if (ret != EPHIDGET_OK)
					return ret;
				packetCount++;
			}
			if (i != len) {
				packetLen = len - i + 5;
				buf[0] = 0;
				pack32(&buf[1], (uint32_t)packetCount);
				memcpy(&buf[5], buffer + i, packetLen - 5);
				ret = PhidgetDevice_transferpacket(bp->iop, (PhidgetDeviceHandle)phid, PHIDGETUSB_REQ_BULK_WRITE, 0, 0, buf, &packetLen, 5000);
				if (ret != EPHIDGET_OK)
					return ret;
			}

			phid->dropCount++;
		}
	}
	return ret;
}


//CRC16 function found from https://ctlsys.com/support/how_to_compute_the_modbus_rtu_message_crc/
// Compute the MODBUS RTU CRC
uint16_t ModRTU_CRC(uint8_t *buf, uint16_t len)
{
	uint16_t crc = 0xFFFF;

	for (uint16_t pos = 0; pos < len; pos++) {
		crc ^= (uint16_t)buf[pos];          // XOR byte into least sig. byte of crc

		for (int i = 8; i != 0; i--) {    // Loop over each bit
			if ((crc & 0x0001) != 0) {      // If the LSB is set
				crc >>= 1;                    // Shift right and XOR 0xA001
				crc ^= 0xA001;
			} else                            // Else LSB is not set
				crc >>= 1;                    // Just shift right
		}
	}
	// Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
	return crc;
}
